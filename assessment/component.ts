import { Component, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { AppBaseView } from "~/baseview";
import { DataService } from "~/services/dataservice";
import { FileHelper } from "~/services/file-helper";
/* end */

import * as app from "tns-core-modules/application";

@Component({
    selector: "assessment",
    templateUrl: "./component.html"
})
export class AssessmentComponent extends AppBaseView implements OnInit, OnDestroy {

    text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
    formatterDate: any;
    formatterTime: any;
    todayDate = new Date();
    curDate: Date = null;
    curTime: Date = null;

    dummyData = [
        {
            text: "Option 1",
            isSelected: false,
            imageSrc: "~/img/ben.png"
        },
        {
            text: "Option 2",
            isSelected: false
        },
        {
            text: "Option 3",
            isSelected: false
        }];

    radioData = this.dummyData;
    textareaValue: string = "";

    progressCurrVal = 10;
    progressMaxVal = 100;

    selectValues = [
        { name: "1", value: "1" },
        { name: "5", value: "5" },
        { name: "10", value: "10" },
        { name: "15", value: "15" },
        { name: "20", value: "20" },
        { name: "25", value: "25" },
        { name: "30", value: "30" },
        { name: "35", value: "35" },
        { name: "40", value: "40" },
        { name: "45", value: "45" },
        { name: "50", value: "50" },
        { name: "55", value: "55" },
        { name: "60", value: "60" }
    ];

    showItemDummy = [
        {
            imageSrc: "~/img/ben.png",
            title: "Drink Some Water",
            isAssigned: false,
            schedule: [
                { frequency: "Daily", time: "10:00 AM"},
                { frequency: "Daily", time: "11:00 AM"},
                { frequency: "Daily", time: "12:30 AM"},
                { frequency: "Daily", time: "01:45 PM"}
            ]
        },
        {
            imageSrc: "~/img/profile_settings_img.png",
            title: "Take A Walk",
            isAssigned: true,
            // schedule: [
            //     { frequency: "Daily", time: "06:00 AM"},
            //     { frequency: "Daily", time: "11:00 AM"},
            //     { frequency: "Daily", time: "04:30 PM"},
            //     { frequency: "Daily", time: "10:45 PM"}
            // ]
        },
    ]

    showItemData = this.showItemDummy;

    constructor(
        public dataservice: DataService,
        private page: Page,
        protected routerExtensions: RouterExtensions,
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        // Use the component constructor to inject providers.
        page.actionBarHidden = true;

        this.formatterDate = new intl.DateTimeFormat('en-US', { 'year': 'numeric', 'month': 'long', 'day': 'numeric' });
        this.curDate = this.formatterDate.format(this.todayDate);
        this.curTime = new Date();


    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    onNavigateTo() {

        super.onNavigateTo();

        this.dataservice.menubar.title = L("assessment_title");
        this.dataservice.menubar.burgerVisible = false;

        this.refreshData();
    }

    onNavigateFrom() {
        super.onNavigateFrom();
    }

    refreshData() {
        super.refreshData();
    }

}
