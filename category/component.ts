import { Component, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { screen } from "tns-core-modules/platform";
/* for the constructor and base class */
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { AppBaseView } from "~/baseview";
import { DataService } from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";

@Component({
  selector: "Category",
  templateUrl: "./component.html"
})
export class CategoryComponent extends AppBaseView
  implements OnInit, OnDestroy {
  categoryId: string;
  shows: Array<any> = [];
  
  // we have a shows array.  I provided it on purpose
  // src = "~/img/icons/down-arrow_grey-medium.png";
  // name: string;
  // isExpanded: boolean = false; // this isn't being used

  constructor(
    private route: ActivatedRoute,
    public dataservice: DataService,
    private page: Page,
    protected routerExtensions: RouterExtensions,
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef
  ) {
    super(page, routerExtensions, zone, changeDetector);
    // Use the component constructor to inject providers.
    page.actionBarHidden = true;
    this.onNavigateTo();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.route.params.forEach(params => {
      // this.pageRoute.activatedRoute.switchMap(activatedRoute => activatedRoute.params).forEach(params => {
      this.categoryId = params.name;
      this.refreshData();
    });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  refreshData() {
    super.refreshData();

    this.dataservice.profileManager
      .getShowsForCategory(this.categoryId)
      .then(shows => {
        this.zone.run(() => {
          this.shows = shows;
        });
      });
  }

  onNavigateTo() {
    //console.log("onNavigateTo Category");
    super.onNavigateTo();

    this.dataservice.menubar.title = "";
    this.dataservice.menubar.burgerVisible = false;
  }

  onNavigateFrom() {
    super.onNavigateFrom();
  }

  
  // camel case please
  onSort(event)
  {
    console.log("Clicked on Sort!");

  }
  
    
  onFavoriteTap(args) {
    var show = args.show;
    var isFav = args.isFav;
    
    for (var ashow of this.shows) {
      if (ashow.showId == show.showId) {
        ashow.isFavorite = isFav;        
      }
    } 
  }
}
