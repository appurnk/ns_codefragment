import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { screen, device } from "tns-core-modules/platform";

import { ActivatedRoute } from "@angular/router";
/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { AppBaseView } from "~/baseview";
import { DataService } from "~/services/dataservice";

var intl = require("nativescript-intl");
import * as app from "tns-core-modules/application";

@Component({
  selector: "ns-category-item",
  templateUrl: "./component.html"
})
export class CategoryItemComponent extends AppBaseView implements OnInit, OnDestroy {
  _show: any;
  isExpanded: boolean = false;
  src = "~/img/icons/down-arrow_grey-medium.png";
  @Input() showSchedule = false;
  @Output() favoriteChange: EventEmitter<any> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    public dataservice: DataService,
    private page: Page,
    protected routerExtensions: RouterExtensions,
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef
  ) {
    super(page, routerExtensions, zone, changeDetector);
    // Use the component constructor to inject providers.
    page.actionBarHidden = true;
    this._show={};
    this.onNavigateTo();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
    
  get show() {
      return this._show;
  }
  @Input()
  set show(inShow) {
    if (inShow!=null) {
      this.dataservice.profileManager.getShowForCategory(inShow.id).then(show => {
        this.zone.run(() => {
          inShow.segments = show.segments;
          this.dataservice.profileManager.normalizeShow(inShow);
          this._show = inShow;
        })
      });
    }
  }

  refreshData() {
    super.refreshData();
  }

  onNavigateTo() {
    //console.log("onNavigateTo Category");
    super.onNavigateTo();

    this.dataservice.menubar.title = "";
    this.dataservice.menubar.burgerVisible = true;
  }

  onNavigateFrom() {
    super.onNavigateFrom();
  }

  onExpand() {

    if (this.src === "~/img/icons/down-arrow_grey-medium.png") {
      this.src = "~/img/icons/up-arrow_grey-medium.png";
    } else {
      this.src = "~/img/icons/down-arrow_grey-medium.png";
    }
    this.isExpanded = !this.isExpanded;

  }
  setFavButton(show, isFav) {
    if (!show.showId) return;
    
    
    this.dataservice.setFavorites(show.showId, isFav).then((data) => {
        show.isFavorite =  isFav;
        this.favoriteChange.emit({show: show, isFav: isFav});
    }).catch(() => {
        console.log("could not set fav");
    });
  }

  triggerButton(arg) {
    //console.log("Clicked on " + arg);
    //console.dir(JSON.stringify(this.show));
    
    var path = null;
    if (arg=="settings" && this.show) {
      path = [
        "/show-settings",
        this.show.showId
      ];
    }
    if (path!=null) {
        this.routerExtensions.navigate(path, {
            transition: {
                name: "fade"
            }
        });
    }
  }

  formatDate(date) {
    var s  = null;
    if (date) {
      s =  (new intl.DateTimeFormat(device.language + "-" + device.region, null, "M/d/yyyy")).format(date);
    }
    return s;
  }
    
  scheduleFormat() {
      if (this.show && this.show.scheduledStart) {
          var time = this.show.scheduledStart.getTime();
          var s: string = (new intl.DateTimeFormat(device.language + "-" + device.region, { 'hour': 'numeric', 'minute': 'numeric' })).format(time);

          while (s.startsWith("0")) s = s.substring(1);
          
          s += " | " + Math.round(this.show.duration/60) + " " + L('category_minutes');

          return s;
      } else {
          return null;
      }
  }
  
  description() {
    var show = this.show;
    if (show.description && show.description.length) {
      return show.description;
    }
    return null;
  }
}
