import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { CategoryComponent } from "./component";
import { CategoryItemComponent } from "./category-item/component";

const routes: Routes = [
    { path: "", component: CategoryComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule],
    declarations: [CategoryItemComponent]
})
export class CategoryRoutingModule { }
