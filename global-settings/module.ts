import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptI18nModule } from "nativescript-ssi-i18n/angular";
import { TNSFontIconModule, TNSFontIconService } from 'nativescript-ngx-fonticon';

import { GlobalSettingsRoutingModule } from "./routing.module";
import { GlobalSettingsComponent } from "./component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        GlobalSettingsRoutingModule,
        
        NativeScriptI18nModule,
		TNSFontIconModule.forRoot({
            'fa': './fonts/fontawesome.css',
            'ion': './fonts/ionicons.css'
		}),
    ],
    declarations: [
        GlobalSettingsComponent
    ],
    providers: [
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class GlobalSettingsModule { }
