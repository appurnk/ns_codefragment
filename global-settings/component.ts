import { Component, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
import {FileHelper} from "~/services/file-helper";
/* end */

import * as app from "tns-core-modules/application";

@Component({
    selector: "GlobalSettings",
    templateUrl: "./component.html"
})
export class GlobalSettingsComponent extends AppBaseView implements OnInit, OnDestroy {
    private _uptime: string = "";
    private _storage: string = "";
    private _contentAnalysis: any;
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        // Use the component constructor to inject providers.
        page.actionBarHidden = true; 
        
        this._contentAnalysis = null;
        this.onNavigateTo(); 
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
     
    ngOnDestroy(): void {
        super.ngOnDestroy(); 
    } 
    
    onNavigateTo() {   
        //console.log("onNavigateTo GlobalSettings"); 
        super.onNavigateTo(); 
         
        this.dataservice.menubar.title=L("global_settings_title"); 
        this.dataservice.menubar.burgerVisible = false;
        
        this.refreshData();
    }
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
    
    refreshData() {
        super.refreshData();
        
        this._uptime = this.settings.uptime;
        this.calculateStorage();
        this.getContentAnalysis();
    }
    
    getContentAnalysis() {
        this.dataservice.profileManager.getContentStats().then((data) => {
            this._contentAnalysis = data;
        }).catch(() => {
            this._contentAnalysis = null;
        });
    }
    
    calculateStorage() {
        var stats = FileHelper.getStatsForPath(FileHelper.getRootOfflinePath());
        
        var s = null;
        if (stats!=null) {
            if (stats.available==0 || stats.total==0) {
                s = null;
            } else {
                s = this.dataservice.humanFileSize(Math.floor(stats.available)) + " / " + this.dataservice.humanFileSize(Math.floor(stats.total));
            }
        }
        
        this._storage = s;
    }
    
    get settings() {
        return this.dataservice.getCompanionSettings();
    }
    
    get companionID() {
        return this.dataservice.CompanionID;
    }
    
    get serialNumber() {
        return this.settings.serialNumber;
    }
    
    get isOffline() {
        return this.settings.offlineContent;
    }
    
    get isShowingDownloadProgress() {
        return this.isOffline;
    }
    
    get downloadProgress() {
        return this.dataservice.downloadProgress;
    }
    
    get isShowingDeleteProgress() {
        return this.dataservice.deleteProgress!=null;
    }
    
    get deleteProgress() {
        return this.dataservice.deleteProgress || "";
    }
    
    get isConnected() {
        return true;
    }
    
    get appID() {
        return this.settings.appId;
    }
    
    get version() {
        return this.settings.version;
    }
    
    get uptime() {
        return this._uptime;
    }
    get isStorageVisible() {
        return this._storage!=null;
    }
    
    get storage() {
        return this._storage || "";
    }
    
    get contentTotal() {
        if (this._contentAnalysis && this._contentAnalysis.totalSize) {
            return this.dataservice.humanFileSize(Math.floor(this._contentAnalysis.totalSize));
        }
        return "";
    }
    
    setOffline(b: boolean) {
        var s = this.dataservice.getCompanionSettings();

        if (b!=s.offlineContent) {
            if (b==false) {
                this.dataservice.contentManager.cancel().then(() => {
                    s.setOfflineContent(b);
    
                    this.dataservice.confirm("delete_content_title", "delete_content_message", "yes", "no").then(val => {
                        if (val) {//
                            this.dataservice.contentManager.deleteAll();
                        }
                    });
                });
            } else {
                this.dataservice.contentManager.cancel().then(() => {
                    this.dataservice.profileManager.checkForOfflineSpace().then(result => {
                        if (result.allow) {
                            this.dataservice.confirm("offline_content_title", "offline_content_message", "yes", "no").then(val => {
                                if (val) {
                                    s.setOfflineContent(b);
                                }
                            });
                        } else {
                            if (result.availableAfter<0) {
                                var spaceStr = this.dataservice.humanFileSize(Math.abs(result.availableAfter));
                                this.dataservice.alert("offline_nospace_title", L("offline_nospace_message", spaceStr), "ok").then(val => {
                                    
                                });
                            }
                        }
                    });
                });
            }
        }


    }
    
    
    
    get isKioskMode() {
        var s = this.dataservice.getCompanionSettings();
        return s.homePage=='home';
    }
    
    setKioskMode(b: boolean) {
        var s = this.dataservice.getCompanionSettings();
        s.setHomeScreen(b?'home':'showList');
    }
    
    get isKioskShowingNews() {
        var s = this.dataservice.getCompanionSettings();
        return s.showNews;
    }
    
    setKiosShowingNews(b: boolean) {
        var s = this.dataservice.getCompanionSettings();
        s.setShowNews(b);
    }
    
    get nightMode() {
        var s = this.dataservice.getCompanionSettings();
        return s.nightOption;
    }
    
    get nightModeValues() {
        return [
            {name: L("global_settings_night_mode_bright"), value: 'clock'},
            {name: L("global_settings_night_mode_dimmed"), value: 'dim'},
            {name: L("global_settings_night_mode_off"), value: 'off'}
        ];
    }
    
    onNightModeChange(val) {
        var s = this.dataservice.getCompanionSettings();
        s.setNightOption(val);
    }
    
    get dimAmount() {
        var s = this.dataservice.getCompanionSettings();
        return s.dimAmount;
    }
    
    onDimAmountChange(val) {
        var s = this.dataservice.getCompanionSettings();
        s.setDimAmount(val);
    }
}
