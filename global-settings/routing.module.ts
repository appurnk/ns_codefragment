import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { GlobalSettingsComponent } from "./component";

const routes: Routes = [
    { path: "", component: GlobalSettingsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class GlobalSettingsRoutingModule { }
