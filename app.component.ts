import { Component, ChangeDetectorRef, NgZone, OnInit, OnDestroy } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { Page } from "tns-core-modules/ui/page";
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
import * as application from "tns-core-modules/application";

import { filter } from "rxjs/operators";
import * as app from "tns-core-modules/application";

import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";

var android: any;
@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent extends AppBaseView implements OnInit, OnDestroy {
    private _sideDrawerTransition: DrawerTransitionBase;
    private _categories: Array<any> = [];
    private _accountName: string = "";
    private memoryInterval: number = 0;

    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        // Use the component constructor to inject services.
        console.log("formFactorx: "+this.formfactor); 
        dataservice.menubar.title="test"; 
        application.on("profileUpdated", this.profileChanged, this);
    } 
    
    outputAndroidMemory() {
        if (isAndroid) {
            var used = java.lang.Runtime.getRuntime().totalMemory() - java.lang.Runtime.getRuntime().freeMemory();
            console.log("used: " + used + "  free: " + java.lang.Runtime.getRuntime().freeMemory() + "  total: " + java.lang.Runtime.getRuntime().totalMemory() + "  max: " + java.lang.Runtime.getRuntime().maxMemory())
        }
    }

    ngOnInit(): void {
        super.ngOnInit();
        
        this._sideDrawerTransition = new SlideInOnTopTransition();
        
        this.setImmersiveModeNow();
        
        if (this.dataservice.getCompanionSettings().customerId<=0) {
            this.routerExtensions.navigate(['/profiles-add'], { clearHistory: true, animated: false });
            return; 
        }
        this.refreshData();

        this.outputAndroidMemory();
        this.memoryInterval = setInterval(() => {
            this.outputAndroidMemory();
        }, 15000);
    }
    
    ngOnDestroy() {
        if (this.memoryInterval) {
            clearInterval(this.memoryInterval);
            this.memoryInterval = 0;
        }
        super.ngOnDestroy();
    }
    
    public refreshData() {
        this._accountName = this.dataservice.getCompanionSettings().getDisplayFullName();
        var user = this.dataservice.getUser();
        if (user!=null) {
            this._accountName = user.displayName;
        }
        
        this.dataservice.profileManager.getShowCategories().then(cats => {
            this._categories = cats;
        });
    }
    
    profileChanged() {
        this.refreshData();
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this.activatedUrl === url;
    } 

    onNavItemTap(navItemRoute: string): void {
        var clearHistory = (navItemRoute=="/home");
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            },
            clearHistory: clearHistory
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    onDrawerButtonTap(): void {
        this.refreshData();
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    
    onCloseMenu(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
    
    onCastClick(): void {
        console.log("onCastClick");
    }
    
    onContactClick(): void {
        this.onNavItemTap("/contact");
    }
    
    onSearchClick(): void {
        console.log("onSearchClick");
    }
    
    onSettingsClick(): void {
        //this.onNavItemTap("/global-settings")
        this.onNavItemTap("/profiles-settings")
    }
    
    onBackClick(): void {
        this.routerExtensions.back();
    }
    
    get dimmedClass() {
        var s = this.dataservice.getCompanionSettings();
        var d = s.dimAmount;
        var nm = s.nightOption;
        
        if (nm=='dim' && (0 || this.dataservice.isNight())) {
            return "dimmed-"+d;
        }
        return "";
    }
}
