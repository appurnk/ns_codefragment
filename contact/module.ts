import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptI18nModule } from "nativescript-ssi-i18n/angular";
import { TNSFontIconModule, TNSFontIconService } from 'nativescript-ngx-fonticon';

import { ContactRoutingModule } from "./routing.module";
import { ContactComponent } from "./component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ContactRoutingModule,
        
        NativeScriptI18nModule,
		TNSFontIconModule.forRoot({
            'fa': './fonts/fontawesome.css',
            'ion': './fonts/ionicons.css'
		}),
    ],
    declarations: [
        ContactComponent
    ],
    providers: [
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ContactModule { }
