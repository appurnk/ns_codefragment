import { Component, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

/* for the constructor and base class */
import { Page, layout } from "tns-core-modules/ui/page";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { AppBaseView } from "~/baseview";
import { DataService } from "~/services/dataservice";
/* end */

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import * as app from "tns-core-modules/application";

@Component({
    selector: "Contact",
    templateUrl: "./component.html"
})
export class ContactComponent extends AppBaseView implements OnInit, OnDestroy {
    public dateFormat = new intl.DateTimeFormat(device.language + "-" + device.region, null, "MMMM d h:mm a");
    // public existingRequest: any = {};
    public existingRequest: any = null;
    public gotExisting: boolean = false;


    // holder for all reasons we receive from server
    public allReasons: any = [];

    dialogOpen = false; // for modal

    radioClass = "contact-radio-button";

    constructor(
        public dataservice: DataService,
        private page: Page,
        protected routerExtensions: RouterExtensions,
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        // Use the component constructor to inject providers.
        page.actionBarHidden = true;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    onNavigateTo() {
        //console.log("onNavigateTo Contact");
        super.onNavigateTo();

        this.dataservice.menubar.title = L("contact_request");
        this.dataservice.menubar.burgerVisible = false;

        this.refreshData();
    }

    onNavigateFrom() {
        super.onNavigateFrom();
    }

    refreshData() {
        super.refreshData();
        this.allReasons = [];
        this.gotExisting = false;
        
        this.dataservice.getContactReasons().then(data => {
            var allReasons = [];
            for (var i = 0; i < data.length; i++) {
                data[i].img = this.dataservice.getContentPath(data[i].img);
                data.selected = false;
                data[i].other = "";
                //data[i].text += " " + data[i].text;
                //data[i].isOther = (i%2==0);
                allReasons.push(data[i]);
            }
            this.allReasons = allReasons;
            console.log("All reasons" + JSON.stringify(this.allReasons));

        });
        this.dataservice.getCurrentContactRequest().then(data => {
            this.gotExisting = true;
            if (data && data.id) {
                data.requestDtmStr = this.dateFormat.format(data.requestDtm);
                this.existingRequest = data;
            } else {
                this.existingRequest = null;
            }
            console.log("this.existingRequest" + JSON.stringify(this.existingRequest));
        });
        console.log(this.existingRequest);
    }

    answerTap(answer) {

        for (var i = 0; i < this.allReasons.length; i++) {
            this.allReasons[i].selected = (this.allReasons[i].id == answer.id);
        }
        if (!answer.isOther) {
            this.dataservice.hideKeyboard();
        }
        console.log("Anser Tap " + JSON.stringify(answer));
        //console.log(this.otherReason);

    }

    submitAnswer() {
        var selectedAnswer = null;
        var otherReason;
        var isOtherAnswer = false;

        for (var i = 0; i < this.allReasons.length; i++) {
            if (this.allReasons[i].selected) {
                selectedAnswer = this.allReasons[i];
                if (selectedAnswer.isOther) {
                    otherReason = selectedAnswer.other.trim();
                    isOtherAnswer = true;
                }
                break;
            } 
        }

        if (selectedAnswer != null && selectedAnswer.id && (!isOtherAnswer || (isOtherAnswer && otherReason.length))) {
            this.dataservice.submitContactRequest("video", selectedAnswer.id, selectedAnswer.isOther, otherReason).then(data => {
                this.existingRequest = data;
                this.existingRequest.requestDtmStr = this.dateFormat.format(this.existingRequest.requestDtm);
                console.log("this.existingRequest " + JSON.stringify(this.existingRequest));
                this.dialogOpen = true;
            });
            console.log("Submitted Request");
        } else {
            this.dataservice.alert("contact_incomplete_title", "contact_incomplete_message", "ok");
        }
    }

    cancelExistingRequest() {
        this.dataservice.confirm("contact_cancel_title", "contact_cancel_message", "yes", "no").then(val => {
            if (val) {
                this.dataservice.cancelContactRequest(this.existingRequest.id).then(data => {
                    this.routerExtensions.back();
                });
            }
        });
    }

    contactTitle() {
        if (this.existingRequest == null) {
            return L("contact_help_title");
        } else {
            return L("contact_request_pending_title")
        }
    }

    contactSubtext() {
        if (this.existingRequest == null) {
            return L("contact_request_text");
        } else {
            return L("contact_request_pending_text")
        }
    } 

    formatSubject(existingRequest) {
        return L("contact_subject") + " " + ((existingRequest.reasonOther.length == 0) ? existingRequest.reason : existingRequest.reasonOther);
    }

    formatSubmit(existingRequest) {
        return L("contact_submitted") +" " + existingRequest.requestDtmStr;
    }

    closeDialog() {
        this.dialogOpen = false;
    }
}
