import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";

@Component({
    moduleId: module.id,
    selector: "category-bands",
    templateUrl: "./component.html"
})
export class CategoryBandsComponent extends AppBaseView implements OnInit, OnDestroy {
    private rotateFocus: boolean = false;
    private _categories: any;
    private _focusIndex: number = 0;
    private _focused: boolean = false;
    private _parentFocused: boolean = false;
    private checkFocusVisibleInterval = 0;
    @Input('scroller') scrollerName: string = "pageContentScroller";
    @Output() favoriteChange: EventEmitter<any> = new EventEmitter();
    @Output() categoryTap: EventEmitter<any> = new EventEmitter();
    @Output() showTap: EventEmitter<any> = new EventEmitter();
    @Output() focusOut: EventEmitter<any> = new EventEmitter();
    @Output() focus: EventEmitter<any> = new EventEmitter();
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        
        this.refreshData();
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        if (this.checkFocusVisibleInterval) {
            clearInterval(this.checkFocusVisibleInterval);
            this.checkFocusVisibleInterval = 0;
        }
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
        
        if (this.checkFocusVisibleInterval) {
            clearInterval(this.checkFocusVisibleInterval);
            this.checkFocusVisibleInterval = 0;
        }
        this.checkFocusVisibleInterval = setInterval(() => {
            if (this.allowFocus) {
                if (!this.isVisibleIndex(this.focusIndex)) {
                    if (!this.focusPrevious()) {
                        if (!this.focusNext()) {
                            this.focusOut.emit(-1);
                        }
                    }
                } else {
                    this.scrollToScheduledIndex(this._focusIndex, false);
                }
            }
        }, 500);
    } 
    
    onNavigateFrom() {
        super.onNavigateFrom();
        
        if (this.checkFocusVisibleInterval) {
            clearInterval(this.checkFocusVisibleInterval);
            this.checkFocusVisibleInterval = 0;
        }
    }
    
    refreshData() { 
    }
    
    get allowFocus() {
        return (this.parentFocused && this.focused && this.isAllowingKeyNavigation());
    }
    
    get focused() {
        return this._focused;
    }
    @Input()
    set focused(focused) {
        var oldAllow = this.allowFocus;
        this._focused = focused;
        
        // check if it has become focused
        if (this.allowFocus && !oldAllow) {
            this.scrollToScheduledIndex(this._focusIndex);
        }
    }
    
    get parentFocused() {
        return this._parentFocused;
    }
    @Input()
    set parentFocused(parentFocused) {
        var oldAllow = this.allowFocus;
        this._parentFocused = parentFocused;
        
        // check if it has become focused
        if (this.allowFocus && !oldAllow) {
            this.scrollToScheduledIndex(this._focusIndex);
        }
    }
    
    get categories() {
        return this._categories;
    }
    @Input()
    set categories(categories) {
        this.zone.run(() => {
            for (var i=0; i<categories.length; i++) {
                categories[i].index = i;
            }
            this._categories = categories;
            setTimeout(() => {
                this._focusIndex = -1;
                this.focusNext();
            }, 10);
        })
    }
    
    get focusIndex() {
        return this._focusIndex;
    }
    set focusIndex(focusIndex) {
        if (this.categories.length) {
            if (this.rotateFocus) {
                focusIndex = focusIndex % this.categories.length;
                while (focusIndex<0) focusIndex += this.categories.length;
            } else {
                if (focusIndex>(this.categories.length-1)) {
                    focusIndex = this.categories.length - 1;
                }
                if (focusIndex<0) {
                    focusIndex = 0;
                }
            }
        }
        
        this.zone.run(() => {
            this._focusIndex = focusIndex;
            for (let cat of this.categories) {
                cat.focused = (cat.index==focusIndex);
                if (cat.focused) {
                    console.log("FOCUS: "+cat.id);
                }
            }
            if (this.allowFocus) {
                this.scrollToScheduledIndex(this._focusIndex);
                console.log("_focusIndex: "+this._focusIndex);
            }
        });
    }
    
    onKeyEvent(keycode: number, action: string) {
        super.onKeyEvent(keycode, action);
        if (this.allowFocus) {
            if (keycode==KeyCodes.up) {
                if (this.focusIndex==0) {
                    this.focusOut.emit(-1);
                } else {
                    if (!this.focusPrevious()) {
                        this.focusOut.emit(-1);
                    }
                }
            }  
            if (keycode==KeyCodes.down) {
                if (this.focusIndex==(this.categories.length-1)) {
                    this.focusOut.emit(1);
                } else {
                    if (!this.focusNext()) {
                        this.focusOut.emit(1);
                    }
                }
            } 
        }
    } 
    
    focusNext() {
        var idx = this._focusIndex;
        
        while (true) {
            idx++;
            if (idx>=this.categories.length) {
                return false;
            }
            if (this.isVisibleIndex(idx)) {
                this.focusIndex = idx;
                return true;
            }
        }
    }
    
    focusPrevious() {
        var idx = this._focusIndex;
        
        while (true) {
            idx--;
            if (idx<0) {
                return false;
            }
            if (this.isVisibleIndex(idx)) {
                this.focusIndex = idx;
                return true;
            }
        }
    }

    scrollToScheduledIndex(index, animated=true) {
        var scrolled = false;
        var tile = this.page.getViewById<View>(this.uuid+"_category_"+this.categories[index].id);
        var scroller = this.page.getViewById<ScrollView>(this.scrollerName);
        if (tile != null && scroller) {
            var pt = tile.getLocationRelativeTo(scroller);
            if (pt) {
                var y = scroller.verticalOffset + pt.y; 
                scroller.scrollToVerticalOffset(y, animated);
                scrolled= true; 
            } 
        } 
        
        if (!scrolled) {
            setTimeout(() => {
                this.scrollToScheduledIndex(index, animated);
            }, 200);
        }
    }
    
    isVisible(category) {
        return category.count>0;
    }
    
    isVisibleIndex(idx) {
        if (idx<0 || idx>=(this.categories.length)) {
            return false;
        }
        return this.isVisible(this.categories[idx]);
    }
    
    onCategoryTap(args) {
        this.categoryTap.emit(args);
    }
    
    onFavoriteTap(args) {
        this.favoriteChange.emit(args);
    }
    
    onShowTap(cat, args) {
        for (var i=0; i<this.categories.length; i++) {
            if (this.categories[i].id==cat.id) {
                this.focusIndex = i;
            }
        }
        this.showTap.emit(args);
        this.focus.emit();
    }
}
