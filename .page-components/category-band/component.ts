import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";

@Component({
    moduleId: module.id,
    selector: "category-band",
    templateUrl: "./component.html"
})
export class CategoryBandComponent extends AppBaseView implements OnInit, OnDestroy {
    private rotateFocus: boolean = false;
    private _category: any;
    private _focusIndex: number = 0;
    private checkShowIndexes: number = 0;
    @Input() parentFocused: boolean = false;
    @Output() favoriteChange: EventEmitter<any> = new EventEmitter();
    @Output() categoryTap: EventEmitter<any> = new EventEmitter();
    @Output() showTap: EventEmitter<any> = new EventEmitter();
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        
        this.refreshData();
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        if (this.checkShowIndexes) {
            clearInterval(this.checkShowIndexes);
            this.checkShowIndexes = 0;
        }
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
        
        if (this.checkShowIndexes) {
            clearInterval(this.checkShowIndexes);
            this.checkShowIndexes = 0;
        }
        this.checkShowIndexes = setInterval(() => {
            if (this.category && this.category.shows) {
                this.updateShowIndexes(this.category.shows);
            }
        }, 500);
    } 
    
    onNavigateFrom() {
        if (this.checkShowIndexes) {
            clearInterval(this.checkShowIndexes);
            this.checkShowIndexes = 0;
        }
        super.onNavigateFrom();
    }
    
    refreshData() { 
    }
    
    get category() {
        return this._category;
    }
    @Input()
    set category(category) {
        this.updateShowIndexes(category.shows);
        this.zone.run(() => {
            
            this._category = category;
            setTimeout(() => {
                this.focusIndex = 0;
            }, 10);
        })
    }
    
    get focusIndex() { 
        return this._focusIndex;
    }
    set focusIndex(focusIndex) {
        if (this.category.shows.length) {
            if (this.rotateFocus) {
                focusIndex = focusIndex % this.category.shows.length;
                while (focusIndex<0) focusIndex += this.category.shows.length;
            } else {
                if (focusIndex>(this.category.shows.length-1)) {
                    focusIndex = this.category.shows.length - 1;
                }
                if (focusIndex<0) {
                    focusIndex = 0;
                }
            }
        }
        
        this.zone.run(() => {
            this._focusIndex = focusIndex;
            if (this.allowFocus) {
                this.scrollToScheduledIndex(this._focusIndex);
                console.log("_focusIndex: "+this._focusIndex);
            }
        });
    }
    
    get allowFocus() {
        return (this.isAllowingKeyNavigation() && this.parentFocused && this.category.focused);
    }
    
    updateShowIndexes(shows) {
        if (shows!=null && shows.length) {
            for (var i=0; i<shows.length; i++) {
                var show = shows[i];
                var key = this.uuid+'_index';
                if (show[key]!=i) {
                    show[key]=i;
                }
            }
        }
    }
    
    isItemFocused(show) {
        return (this.allowFocus && show[this.uuid+'_index']==this.focusIndex);
    }
    
    onKeyEvent(keycode: number, action: string) {
        super.onKeyEvent(keycode, action);
        if (this.allowFocus) {
            if (keycode==KeyCodes.left) {
                this.focusIndex--;
            } 
            if (keycode==KeyCodes.right) {
                this.focusIndex++;
            } 
            if (keycode==KeyCodes.select) {
                this.onShowTap({show: this.category.shows[this.focusIndex], keycode: keycode});
            }
            
            console.log("KEY: "+keycode);
        }
    }

    scrollToScheduledIndex(index) {
        var scrolled = false;
        var tile = this.page.getViewById<View>(this.uuid+"_show_"+index);
        var scroller = this.page.getViewById<ScrollView>(this.uuid+"_scroller");
        if (tile != null && scroller) {
            var pt = tile.getLocationRelativeTo(scroller);
            if (pt) {
                var y = scroller.horizontalOffset + pt.x; 
                scroller.scrollToHorizontalOffset(y, true);
                scrolled= true; 
            }
        } 
        
        if (!scrolled) {
            setTimeout(() => {
                this.scrollToScheduledIndex(index);
            }, 200);
        }
    }
    
    onCategoryTap() {
        this.categoryTap.emit(this.category.id);
    }
    
    onShowTap(show) {
        this.focusIndex = show.index;
        this.showTap.emit({show: show, keycode: KeyCodes.playpause});
    }
    
    onFavoriteTap(show, isFav) {
        if (!show.showId) return;
        
        this.favoriteChange.emit({show: show, isFav: isFav});
    }
    
    scheduleFormat(show) {
        if (show && show.scheduledStart) {
            var time = show.scheduledStart.getTime();
            var s: string = (new intl.DateTimeFormat(device.language + "-" + device.region, { 'hour': 'numeric', 'minute': 'numeric' })).format(time);
    
            while (s.startsWith("0")) s = s.substring(1);
    
            return s;
        } else {
            return null;
        }
    }
}
