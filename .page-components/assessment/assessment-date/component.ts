import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import { Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";
const ModalPicker = require("nativescript-modal-datetimepicker-ssi").ModalDatetimepicker;

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { AppBaseView } from "~/baseview";
import { DataService } from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: "assessment-date",
    templateUrl: "./component.html"
})
export class AssessmentDateComponent extends AppBaseView implements OnInit, OnDestroy {

    formatter: any;
    picker: any;

    @Output() changed: EventEmitter<any> = new EventEmitter();
    private _dateTitle: string = "";

    private _value: Date = null;
    // @Input() visibility: boolean = true;

    constructor(
        public dataservice: DataService,
        private page: Page,
        protected routerExtensions: RouterExtensions,
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        this.picker = new ModalPicker();
        this.formatter = new intl.DateTimeFormat('en-US', { 'year': 'numeric', 'month': 'long', 'day': 'numeric' });
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    onNavigateTo() {
        super.onNavigateTo();
    }

    onNavigateFrom() {
        super.onNavigateFrom();
    }

    refreshData() {
    }

    get value() {
        return this._value;
    }
    @Input()
    set value(value) {
        this.zone.run(() => {
            this._value = value;
        });
    }
    get valueString() {
        if (this._value == null) {
            return "";
        }

        return this.formatter.format(this._value);
    }

    get dateTitle() {
        return this._dateTitle;
    }
    @Input()
    set dateTitle(title) {
        this.zone.run(() => {
            this._dateTitle = title;
        });
    }

    pickDate() {
        this.dataservice.hideKeyboard();
        var minDate = new Date(1900, 0, 1);

        this.picker.pickDate({
            theme: "light",
            minDate: new Date(),
        }).then((result) => {
            if (result && result.year) {
                var d = new Date(result.year, result.month - 1, result.day);
                this._value = d;
                this.changed.emit(d);
            }
        }).catch((error) => {
            console.log("Error: " + error);
        });
    }

}
