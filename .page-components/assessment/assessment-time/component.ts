import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import { Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";
const ModalPicker = require("nativescript-modal-datetimepicker-ssi").ModalDatetimepicker;

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { AppBaseView } from "~/baseview";
import { DataService } from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: "assessment-time",
    templateUrl: "./component.html"
})
export class AssessmentTimeComponent extends AppBaseView implements OnInit, OnDestroy {

    formatter: any;
    outFormatter: any;
    picker: any;

    @Output() changed: EventEmitter<any> = new EventEmitter();
    private _timeTitle: string = "";

    private _timeValue: Date = null;
    // @Input() visibility: boolean = true;

    constructor(
        public dataservice: DataService,
        private page: Page,
        protected routerExtensions: RouterExtensions,
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        this.picker = new ModalPicker();
        this.formatter = new intl.DateTimeFormat(device.language + "-" + device.region, null, "h:mm a");
        this.outFormatter = new intl.DateTimeFormat(device.language + "-" + device.region, null, "H:mm");
    }

    get timeTitle() {
        return this._timeTitle;
    }
    @Input()
    set timeTitle(title) {
        this.zone.run(() => {
            this._timeTitle = title;
        });
    }

    get  timeValue() {
        return this.formatter.format(this._timeValue);
    }
    @Input()
    set timeValue(value) {

        this.zone.run(() => {
            this._timeValue = value;
        });
    }
    get valueString() {
        if (this._timeValue == null) {
            return "";
        }
        
        return this.formatter.format(this._timeValue);
    }

    pickTime() {
        var hour = 0;
        var min = 0;
        if (this._timeValue != null) {
            hour = this._timeValue.getHours();
            min = this._timeValue.getMinutes();
        } else {
            var now = new Date();
            hour = now.getHours();
            min = now.getMinutes();
        }

        var options = {
            startingHour: hour,
            startingMinute: min
        };

        this.picker.pickTime(options).then((result) => {
            if (result) {
                var d = new Date();
                d.setHours(result.hour);
                d.setMinutes(result.minute);
                d.setSeconds(0);
                d.setMilliseconds(0);
                this._timeValue = d;
                this.changed.emit(d);
            }
        }).catch((error: string) => {
            console.log("Error: " + error);
        });
    }

}
