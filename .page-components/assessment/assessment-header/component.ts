import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";

@Component({
    moduleId: module.id,
    selector: "assessment-header",
    templateUrl: "./component.html"
})
export class AssessmentHeaderComponent extends AppBaseView implements OnInit, OnDestroy {

    @Output() press: EventEmitter<any> = new EventEmitter();
    private _headerText: string = "";

    constructor(
        public dataservice: DataService,
        private page: Page,
        protected routerExtensions: RouterExtensions,
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
    }
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
    
    refreshData() {
    }

    get headerText() {
        return this._headerText;
    }
    @Input()
    set headerText(title) {
        this.zone.run(() => {
            this._headerText = title;
        });
    }

    onTap() {
        console.log("header tap");
        this.press.emit({});
    }
}
