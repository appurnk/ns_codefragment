import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";

@Component({
    moduleId: module.id,
    selector: "features-carousel",
    templateUrl: "./component.html"
})
export class FeaturesCarouselComponent extends AppBaseView implements OnInit, OnDestroy {
    private _items: Array<any> = [];
    private _focused: boolean = false;
    private _parentFocused: boolean = false;
    @Input('scroller') scrollerName: string = "pageContentScroller";
    @Output('itemTap') tap: EventEmitter<any> = new EventEmitter();
    @Output() titleTap: EventEmitter<any> = new EventEmitter();
    @Output() focusOut: EventEmitter<any> = new EventEmitter();
    
    activeItemInterval: number = 0;
    activeItemDirection: number = 1;
    pagerInterval: number = 10000;
    activeNewsIndex: number = 0;
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        // Use the component constructor to inject providers.
        page.actionBarHidden = true; 
        this.refreshData();
        this.onNavigateTo();
    }
    
    get items() {
        return this._items;
    }
    @Input()
    set items(items) {
        this.zone.run(() => {
            this._items = items;
            setTimeout(() => {
                this.setActiveNewsIndex(0);
            }, 10);
        })
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
        
        if (this.activeItemInterval>0) {
            clearInterval(this.activeItemInterval);
            this.activeItemInterval = 0;
        }
    }
    
    onNavigateTo() {
        super.onNavigateTo();
        
        // restart auto rotate
        this.setActiveNewsIndex(this.activeNewsIndex);
    } 
    
    onNavigateFrom() {
        super.onNavigateFrom();
        
        if (this.activeItemInterval>0) {
            clearInterval(this.activeItemInterval);
            this.activeItemInterval = 0;
        }
    }
    
    refreshData() { 
    }
    
    get allowFocus() {
        return (this.parentFocused && this.focused && this.isAllowingKeyNavigation());
    }
    
    get focused() {
        return this._focused;
    }
    @Input()
    set focused(focused) {
        var oldAllow = this.allowFocus;
        this._focused = focused;
        
        // check if it has become focused
        if (this.allowFocus && !oldAllow) {
            this.makeVisible();
        }
    }
    
    get parentFocused() {
        return this._parentFocused;
    }
    @Input()
    set parentFocused(parentFocused) {
        var oldAllow = this.allowFocus;
        this._parentFocused = parentFocused;
        
        // check if it has become focused
        if (this.allowFocus && !oldAllow) {
            this.makeVisible();
        }
    }

    makeVisible() {
        console.log("NEWS VISIBLE");
        var scrolled = false;
        var tile = this.page.getViewById<View>(this.uuid+"_container");
        var scroller = this.page.getViewById<ScrollView>(this.scrollerName);
        if (tile != null && scroller) {
            var pt = tile.getLocationRelativeTo(scroller);
            if (pt) {
                var y = scroller.verticalOffset + pt.y; 
                scroller.scrollToVerticalOffset(y, true);
                scrolled= true; 
            } 
        } 
        
        if (!scrolled) {
            setTimeout(() => {
                this.makeVisible();
            }, 200);
        }
    }
    
    onKeyEvent(keycode: number, action: string) {
        super.onKeyEvent(keycode, action);
        if (this.allowFocus) {
            if (keycode==KeyCodes.right) {
                this.onNextNews();
            } 
            if (keycode==KeyCodes.left) {
                this.onPreviousNews();
            }  
            if (keycode==KeyCodes.up) {
                this.focusOut.emit(-1);
            }  
            if (keycode==KeyCodes.down) {
                this.focusOut.emit(1);
            }
        }
    } 
    
    setActiveNewsIndex(idx) {
        if (this.activeItemInterval>0) {
            clearInterval(this.activeItemInterval);
            this.activeItemInterval = 0;
        }
        this.activeItemInterval = setInterval(() => {
            this.onActiveNewsInterval();
        }, this.pagerInterval);
        
        
        var transitionTime = 750;
        
        if (this.items.length>0) {
            idx = idx % this.items.length;
            while (idx<0) idx+=this.items.length;
        }
        
        var oldIndex = this.activeNewsIndex;
        this.activeNewsIndex = idx;
        
        for (var i=0; i<this.items.length; i++) {
            this.items[i].selected = (i==this.activeNewsIndex);
        }
        
        if (oldIndex!=idx) {
            var oldItem = this.page.getViewById<View>(this.uuid+"_item_"+oldIndex);
            if (oldItem!=null) {
                oldItem.animate({
                    opacity: 0,
                    duration: transitionTime
                });
            }
        }
        
        var showItem = () => {
            var newItem = this.page.getViewById<View>(this.uuid+"_item_"+idx);
            if (newItem!=null) {
                newItem.animate({
                    opacity: 1,
                    duration: transitionTime
                });
            } else {
                setTimeout(showItem, 50);
            }
        };
        
        showItem();
    }
    
    onActiveNewsInterval() {
        var idx = this.activeNewsIndex + this.activeItemDirection;
        this.setActiveNewsIndex(idx);
    }
    
    onPreviousNews() {
        this.activeItemDirection = -1;
        
        if (this.items.length<=0) return;
        
        var idx = this.activeNewsIndex;
        idx--;
        this.setActiveNewsIndex(idx);
    }
    
    onNextNews() {
        this.activeItemDirection = 1;
        
        if (this.items.length<=0) return;
        
        var idx = this.activeNewsIndex;
        idx++;
        this.setActiveNewsIndex(idx);
    }
    
    onSwipeNews(args: SwipeGestureEventData) {
        if (args.direction == SwipeDirection.left) {
            this.onNextNews();
        }if (args.direction == SwipeDirection.right) {
            this.onPreviousNews();
        }
    }
    
    itemTap() {
        var item = this.items[this.activeNewsIndex];
        console.log(JSON.stringify(item))
        this.tap.emit(item);
    }
    
    onTitleTap() {
        this.titleTap.emit({});
    }
}
