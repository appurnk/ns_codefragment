import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";
const ModalPicker = require("nativescript-modal-datetimepicker-ssi").ModalDatetimepicker;

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: "settings-date",
    templateUrl: "./component.html"
})
export class SettingsDateComponent extends AppBaseView implements OnInit, OnDestroy {
    formatter: any;
    picker: any;
    private _title: string = "";
    private _value: Date = null;
    @Input() visibility: boolean = true;
    @Output() changed: EventEmitter<any> = new EventEmitter();
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        this.picker = new ModalPicker();
        this.formatter = new intl.DateTimeFormat('en-US', {'year': 'numeric', 'month': 'long', 'day': 'numeric'});
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
    } 
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
    
    refreshData() { 
    }
    
    get title() {
        return this._title;
    }
    @Input()
    set title(title) {
        this.zone.run(() => {
            this._title = title;
        })
    }
    
    get value() {
        return this._value;
    }
    get valueString() {
        if (this._value==null) {
            return "";
        }
        return this.formatter.format(this._value);
    }
    @Input()
    set value(value) {
        this.zone.run(() => {
            this._value = value;
        })
    }    

    pickDate() {
        this.dataservice.hideKeyboard();
        var minDate = new Date(1900, 0, 1);
        
        var val = new Date();
        if (this._value!=null) {    
            var mon = this._value.getMonth()+1;
            var date = this._value.getDate();
            var year = this._value.getFullYear();
            var val = new Date(year, mon, date, 0, 0, 0);
        }

        this.picker.pickDate({
            title: this.title,
            theme: "light",
            maxDate: new Date(),
            minDate: minDate,
            startingDate: this._value
        }).then((result) => {
            if (result && result.year) {
                var d = new Date(result.year, result.month-1, result.day);
                this._value = d;
                this.changed.emit(d);
            }
        }).catch((error) => {
            console.log("Error: " + error);
        });
    }
}
