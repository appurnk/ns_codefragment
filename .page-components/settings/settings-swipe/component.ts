import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection, PanGestureEventData } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: "settings-swipe",
    templateUrl: "./component.html"
})
export class SettingsSwipeComponent extends AppBaseView implements OnInit, OnDestroy {
    private _title: string = "";
    private _value: string = "";
    @Input() visibility: boolean = true;
    @Input() actionWidth: number = 200;
    @Input() actionTitle: string = "Delete";
    @Input() actionClass: string = "danger";
    @Output() actionTap: EventEmitter<any> = new EventEmitter();
    @Output() press: EventEmitter<any> = new EventEmitter();
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
    } 
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
    
    refreshData() { 
    }
    
    get title() {
        return this._title;
    }
    @Input()
    set title(title) {
        this.zone.run(() => {
            this._title = title;
        })
    }
    
    get value() {
        return this._value;
    }
    @Input()
    set value(value) {
        this.zone.run(() => {
            this._value = value;
        })
    }
    
    onActionTap() {
        if (this.scrollInterval) {
            return;
        }
        if (this.scrollOffset>0) {
            this.unscroll();
        }
        console.log("action tap");
        this.actionTap.emit({});
    }
    
    onTap() {
        if (this.scrollInterval) {
            return;
        }
        if (this.scrollOffset>0) {
            this.unscroll();
            return;
        }
        console.log("swipe body tap");
        this.press.emit({});
    }
    
    unscroll() {
        if (this.scrollInterval) return;
        
        this.scrollInterval = setInterval(() => {
            var sign = 1;
            if (this.scrollOffset!=0) sign = Math.abs(this.scrollOffset)/this.scrollOffset;
            var step = 7.5 * sign;

            if (Math.abs(this.scrollOffset)!=0) {
                this.scrollOffset -= step;
                if (Math.abs(this.scrollOffset)<step) {
                    this.scrollOffset = 0;
                }
            } else {
                clearInterval(this.scrollInterval);
                this.scrollInterval = 0;
                this.scrollOffset = 0;
            }
        }, 10);
    }
    
    
    scrollInterval: number = 0;
    scrollOffset: number = 0;
    onPan(args: PanGestureEventData) {
        if (this.scrollInterval) return;

        var threshold = this.actionWidth/3;
        this.scrollOffset = Math.floor(-args.deltaX);  // this completely resets the scrolling when button already visible
        if (this.scrollOffset<0) this.scrollOffset = 0;
        if (this.scrollOffset>this.actionWidth) this.scrollOffset = this.actionWidth;
        
        if (Math.abs(this.scrollOffset)>threshold) {
           // this.scrollOffset = this.scrollOffset>0?threshold:(-threshold);
            if (args.state==3) {
                this.scrollInterval = setInterval(() => {
                    var sign = 1;
                    if (this.scrollOffset!=0) sign = Math.abs(this.scrollOffset)/this.scrollOffset;
                    var step = 15 * sign;

                    if (Math.abs(this.scrollOffset)<this.actionWidth) {
                        this.scrollOffset += step;
                        if (Math.abs(this.scrollOffset)>this.actionWidth) {
                            this.scrollOffset = this.actionWidth * sign;
                        }
                    } else {
                        clearInterval(this.scrollInterval);
                        this.scrollInterval = 0;
                    }                     
                }, 10);

                return;
            }
        }
        if (args.state==3) {
            this.scrollInterval = setInterval(() => {
                var sign = 1;
                if (this.scrollOffset!=0) sign = Math.abs(this.scrollOffset)/this.scrollOffset;
                var step = 7.5 * sign;

                if (Math.abs(this.scrollOffset)!=0) {
                    this.scrollOffset -= step;
                    if (Math.abs(this.scrollOffset)<step) {
                        this.scrollOffset = 0;
                    }
                } else {
                    clearInterval(this.scrollInterval);
                    this.scrollInterval = 0;
                    this.scrollOffset = 0;
                }
            }, 10);
        }
/*
        this.deltaX = args.deltaX;
        this.deltaY = args.deltaY;
        this.state = args.state;*/
    }
}
