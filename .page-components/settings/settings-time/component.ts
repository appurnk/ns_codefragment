import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";
const ModalPicker = require("nativescript-modal-datetimepicker-ssi").ModalDatetimepicker;

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: "settings-time",
    templateUrl: "./component.html"
})
export class SettingsTimeComponent extends AppBaseView implements OnInit, OnDestroy {
    formatter: any;
    outFormatter: any;
    picker: any;
    private _title: string = "";
    private _value: Date = null;
    @Input() visibility: boolean = true;
    @Output() changed: EventEmitter<any> = new EventEmitter();
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        this.picker = new ModalPicker();
        this.formatter = new intl.DateTimeFormat(device.language + "-" + device.region, null, "h:mm a");
        this.outFormatter = new intl.DateTimeFormat(device.language + "-" + device.region, null, "H:mm");
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
    } 
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
    
    refreshData() { 
    }
    
    get title() {
        return this._title;
    }
    @Input()
    set title(title) {
        this.zone.run(() => {
            this._title = title;
        })
    }
    
    get value() {
        return this.outFormatter.format(this._value);
    }
    get valueString() {
        if (this._value==null) {
            return "";
        }
        return this.formatter.format(this._value);
    }
    @Input()
    set value(value: string) {
        var val = null;
        if (value!=null) {
            if (value=="now") {
                val = new Date();
                val.setSeconds(0);
                val.setMilliseconds(0);
            } else {
                var hour = 0;
                var min = 0;
                if (value.indexOf(":")>=0) {
                    var vals = (value).split(/[\:\s]+/);
                    hour = parseInt(vals[0]);
                    min = parseInt(vals[1]);
                    if (vals.length>2 && vals[2].startsWith("P")) {
                        hour+=12;
                    }
                } else {
                    var n = parseInt(value);
                    min = n%100;
                    hour = Math.round((n - min)/100);
                    while (min>=60) {
                        min = min - 60;
                        hour++;
                    }
                    hour = hour % 24;
                }
                val = new Date();
                val.setHours(hour);
                val.setMinutes(min);
                val.setSeconds(0);
                val.setMilliseconds(0);
            }
        }
        
        this.zone.run(() => {
            this._value = val;
        })
    }    

    pickTime() {    
        var hour = 0;
        var min = 0;
        if (this._value!=null) {
            hour = this._value.getHours();
            min = this._value.getMinutes();
        } else {
            var now = new Date();
            hour = now.getHours();
            min = now.getMinutes();
        }
        
        var options ={
            startingHour: hour,
            startingMinute: min
        };
        
        this.picker.pickTime(options).then((result) => {
            if (result) {
                var d = new Date();
                d.setHours(result.hour);
                d.setMinutes(result.minute);
                d.setSeconds(0);
                d.setMilliseconds(0);
                this._value = d;
                this.changed.emit(d);
            }
        }).catch((error) => {
            console.log("Error: " + error);
        });
    }
}
