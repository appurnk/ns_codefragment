import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData, SwipeDirection } from "tns-core-modules/ui/gestures";

var intl = require("nativescript-intl");
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

import {Img, FailureEventData } from "nativescript-image";
import { ScrollView } from "tns-core-modules/ui/scroll-view";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";
import { categories } from "tns-core-modules/trace/trace";
import { KeyCodes } from "~/keycodes";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: "settings-slider",
    templateUrl: "./component.html"
})
export class SettingsSliderComponent extends AppBaseView implements OnInit, OnDestroy {
    private _title: string = "";
    private _value: number = 0;
    @Input() min: number = 0;
    @Input() max: number = 100;
    @Input() visibility: boolean = true;
    @Output() changed: EventEmitter<any> = new EventEmitter();
    
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        super.onNavigateTo();
    } 
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
    
    refreshData() { 
    }
    
    get title() {
        return this._title;
    }
    @Input()
    set title(title) {
        this.zone.run(() => {
            this._title = title;
        })
    }
    
    get valueText() {
        return "" + this._value + "%";
    }
    get value() {
        return this._value;
    }
    @Input()
    set value(value) {
        this.zone.run(() => {
            this._value = value;
        })
    }
    
    onSliderValueChange(args) {
        var val = args.object.value;
        if (val!=this._value) {
            this._value = val;
            //console.log("emitting: "+val);
            this.changed.emit(this._value);
        }
    }
}
