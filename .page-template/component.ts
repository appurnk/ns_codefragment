import { Component, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

/* for the constructor and base class */
import { Page } from "tns-core-modules/ui/page";
import { ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {AppBaseView} from "~/baseview";
import {DataService} from "~/services/dataservice";
/* end */

import * as app from "tns-core-modules/application";

@Component({
    selector: "Template",
    templateUrl: "./component.html"
})
export class TemplateComponent extends AppBaseView implements OnInit, OnDestroy {
    constructor(
        public dataservice: DataService,
        private page: Page, 
        protected routerExtensions: RouterExtensions, 
        private zone: NgZone,
        private changeDetector: ChangeDetectorRef) {
        super(page, routerExtensions, zone, changeDetector);
        // Use the component constructor to inject providers.
        page.actionBarHidden = true; 
        this.onNavigateTo();
    }

    ngOnInit(): void {
        super.ngOnInit();
    }
    
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    
    onNavigateTo() {
        //console.log("onNavigateTo Template");
        super.onNavigateTo();
        
        this.dataservice.menubar.title="Template"; 
        this.dataservice.menubar.burgerVisible = true;
    }
    
    onNavigateFrom() {
        super.onNavigateFrom();
    }
}
